RandomizedPropertyTest.jl Changelog
===================================

trunk
-----

- Fixed a Julia 1.0 compatibility issue of tests.
- Improved documentation.

Version 0.1.0
-------------

- This is the initial version.
